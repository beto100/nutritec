package com.betofortera.nutritec3;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by beto on 4/19/15.
 */
public class RecordDetailsListFragment extends ListFragment {

    int[] percentageArray = null;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // get the bundle
        Bundle bundle = getArguments();
        if(bundle!=null) {
            percentageArray = bundle.getIntArray("percentageArray");
        }
        RecordDetailFoodAdapter recDetAdapter = new RecordDetailFoodAdapter(getActivity(),percentageArray);
        setListAdapter(recDetAdapter);
    }
}

// Single Row Class
// ////////////////////////////////////////////////////
class SingleRecordDetailRow
{
    public int image;
    public int percentage;

    SingleRecordDetailRow(int image, int percentage) {
        this.image = image;
        this.percentage = percentage;
    }
}

// Food Adapter
// ////////////////////////////////////////////////////
class RecordDetailFoodAdapter extends BaseAdapter
{
    ArrayList<SingleRecordDetailRow> list;
    Context context;
    int[] foodPercentages;

    public RecordDetailFoodAdapter(Context c, int[] percentages)
    {
        context = c;
        foodPercentages = percentages;
        list = new ArrayList<SingleRecordDetailRow>();

        int[] foodImages = {R.drawable.vegetables,R.drawable.milk,R.drawable.meat,R.drawable.sugar,R.drawable.legumes,R.drawable.fruits,R.drawable.cereal,R.drawable.fats};
        for(int i=0;i<8;i++) {
            list.add(new SingleRecordDetailRow(foodImages[i],foodPercentages[i]));
        }
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    class MyViewHolder
    {
        ImageView foodImage;
        ProgressBar foodProgressBar;
        TextView foodTextPercentage;

        MyViewHolder(View v) {
            this.foodImage = (ImageView) v.findViewById(R.id.foodImageRecordDetail);
            this.foodProgressBar = (ProgressBar) v.findViewById(R.id.progressBar);
            this.foodTextPercentage = (TextView) v.findViewById(R.id.recordDetailPercentage);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;
        MyViewHolder holder = null;
        if(row == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.row_record_detail, parent, false);
            holder = new MyViewHolder(row);
            row.setTag(holder);
        }
        else {
            holder = (MyViewHolder) row.getTag();
        }

        SingleRecordDetailRow temp = list.get(position);
        holder.foodImage.setImageResource(temp.image);
        holder.foodProgressBar.setProgress(temp.percentage);
        holder.foodTextPercentage.setText(temp.percentage+"%");
        return row;
    }
}
