package com.betofortera.nutritec3;


import android.app.Activity;
import android.app.FragmentManager;
import android.app.ListFragment;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by beto on 4/4/15.
 */
public class MyListFoodFragment extends ListFragment implements AdapterView.OnItemClickListener {

    protected FoodAdapter foodAdapter;
    protected int[] foodCounters = null;
    protected int customLayout = -1;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // set the listview adapter
        Bundle bundle = getArguments();
        if(bundle!=null) {
            foodCounters = bundle.getIntArray("dynamicFoodCounter");
            customLayout = bundle.getInt("customLay");
        }
        foodAdapter = new FoodAdapter(getActivity(),foodCounters);
        setListAdapter(foodAdapter);
        getListView().setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        TextView foodTitle = (TextView) view.findViewById(R.id.foodTitle);
        Bundle bundle = new Bundle();
        bundle.putString("foodTitle",foodTitle.getText().toString());
        bundle.putInt("itemPos",position);
        bundle.putInt("customLay",customLayout);

        FragmentManager manager = getActivity().getFragmentManager();
        MyDialogCreate myDialogCreate = new MyDialogCreate();
        myDialogCreate.setArguments(bundle);
        myDialogCreate.show(manager,"DialogCreate");
    }

    public void updateSomeRow(int[] newDataSource)
    {
        foodCounters = newDataSource;
        foodAdapter.notifyDataSetChanged();
    }
}


// Single Row Class
// ////////////////////////////////////////////////////
class SingleRow
{
    public String title;
    public int image;

    SingleRow(String title, int image) {
        this.title = title;
        this.image = image;
    }
}


// Food Adapter
// ////////////////////////////////////////////////////
class FoodAdapter extends BaseAdapter
{
    ArrayList<SingleRow> list;
    Context context;
    int[] foodCounters;

    public FoodAdapter(Context c, int[] counters)
    {
        context = c;
        foodCounters = counters;
        list = new ArrayList<SingleRow>();
        Resources res = c.getResources();
        String[] foodTitles = res.getStringArray(R.array.foodTitles);
        int[] foodImages = {R.drawable.vegetables,R.drawable.milk,R.drawable.meat,R.drawable.sugar,R.drawable.legumes,R.drawable.fruits,R.drawable.cereal,R.drawable.fats};

        for(int i=0;i<8;i++) {
            list.add(new SingleRow(foodTitles[i],foodImages[i]));
        }
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    class MyViewHolder
    {
        ImageView foodImage;
        TextView foodTitle;
        TextView foodCounter;

        MyViewHolder(View v) {
            this.foodImage = (ImageView) v.findViewById(R.id.foodImage);
            this.foodTitle = (TextView) v.findViewById(R.id.foodTitle);
            this.foodCounter = (TextView) v.findViewById(R.id.foodCounter);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;
        MyViewHolder holder = null;
        if(row == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.row_diet, parent, false);
            holder = new MyViewHolder(row);
            row.setTag(holder);
        }
        else {
            holder = (MyViewHolder) row.getTag();
        }

        SingleRow temp = list.get(position);
        holder.foodTitle.setText(temp.title);
        holder.foodImage.setImageResource(temp.image);
        holder.foodCounter.setText(""+foodCounters[position]);

        return row;
    }
}
