package com.betofortera.nutritec3;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class NavigationDrawerFragment extends android.support.v4.app.Fragment {

    private RecyclerView recyclerView;
    private NavDrawerAdapter drawerAdpater;

    public static final String PREF_FILE_NAME = "testpref";
    public static final String USER_LEARNED_DRAWER_KEY = "user_learned_drawer";
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private boolean mUserLearnedDrawer = false;
    private boolean mFromSavedInstanceState = false;
    private View containerView;
    MyDayActivity dayActivity;

    public NavigationDrawerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserLearnedDrawer= Boolean.parseBoolean(readFromPreferences(getActivity(),USER_LEARNED_DRAWER_KEY,"false"));
        if(savedInstanceState != null) {
            mFromSavedInstanceState = true;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        recyclerView = (RecyclerView) layout.findViewById(R.id.drawer_list);
        drawerAdpater = new NavDrawerAdapter(getActivity(),getData());
        recyclerView.setAdapter(drawerAdpater);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        return layout;
    }

    public static List<NavInfoRow> getData()
    {
        List<NavInfoRow> data = new ArrayList<>();
        int[] icons = {R.drawable.mydietnav,R.drawable.myhistorynav,R.drawable.myreferencesnav,R.drawable.myaboutnav};
        String[] titles = {"Mi Dieta", "Mi Historial", "Mis Referencias", "Acerca"};
        for(int i = 0; i < titles.length && i < icons.length; i++) {

            NavInfoRow current  = new NavInfoRow();
            current.iconId = icons[i];
            current.title = titles[i];
            data.add(current);
        }
        return data;
    }


    public void setUp(int fragmentId,DrawerLayout drawerLayout, Toolbar toolBar, final MyDayActivity dayActivity)
    {
        this.dayActivity = dayActivity;
        containerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(),drawerLayout,toolBar,R.string.drawer_open,R.string.drawer_close){

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

                dayActivity.navOpen = true;
                if(!mUserLearnedDrawer) {
                    mUserLearnedDrawer = true;
                    saveToPreferences(getActivity(),USER_LEARNED_DRAWER_KEY,mUserLearnedDrawer+"");
                }
                getActivity().invalidateOptionsMenu();
            }


            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);

                dayActivity.navOpen = false;
                getActivity().invalidateOptionsMenu();
            }
        };
        if(!mUserLearnedDrawer && !mFromSavedInstanceState) {
            mDrawerLayout.openDrawer(containerView);
            this.dayActivity.navOpen = true;
        }
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
    }

    public static void saveToPreferences(Context c, String preferenceName, String preferenceValue)
    {
        SharedPreferences sharedPreferences = c.getSharedPreferences(PREF_FILE_NAME,c.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(preferenceName,preferenceValue);
        editor.apply();
    }

    public static String readFromPreferences(Context c, String preferenceName, String defaultValue)
    {
        SharedPreferences sharedPreferences = c.getSharedPreferences(PREF_FILE_NAME,c.MODE_PRIVATE);
        return sharedPreferences.getString(preferenceName,defaultValue);
    }



}
