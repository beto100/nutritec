package com.betofortera.nutritec3;

/**
 * Created by beto on 4/12/15.
 */
public class DietRegister {

    private int id;
    private String currDate;
    private int por_vegetales;
    private int por_leche;
    private int por_carnes;
    private int por_azucares;
    private int por_legumbres;
    private int por_frutas;
    private int por_cereales;
    private int por_grasas;

    public DietRegister()
    {}

    public DietRegister(int id, String currDate, int por_vegetales, int por_leche, int por_carnes, int por_azucares, int por_legumbres, int por_frutas, int por_cereales, int por_grasas) {
        this.id = id;
        this.currDate = currDate;
        this.por_vegetales = por_vegetales;
        this.por_leche = por_leche;
        this.por_carnes = por_carnes;
        this.por_azucares = por_azucares;
        this.por_legumbres = por_legumbres;
        this.por_frutas = por_frutas;
        this.por_cereales = por_cereales;
        this.por_grasas = por_grasas;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCurrDate() {
        return currDate;
    }

    public void setCurrDate(String currDate) {
        this.currDate = currDate;
    }

    public int getPor_vegetales() {
        return por_vegetales;
    }

    public void setPor_vegetales(int por_vegetales) {
        this.por_vegetales = por_vegetales;
    }

    public int getPor_leche() {
        return por_leche;
    }

    public void setPor_leche(int por_leche) {
        this.por_leche = por_leche;
    }

    public int getPor_carnes() {
        return por_carnes;
    }

    public void setPor_carnes(int por_carnes) {
        this.por_carnes = por_carnes;
    }

    public int getPor_azucares() {
        return por_azucares;
    }

    public void setPor_azucares(int por_azucares) {
        this.por_azucares = por_azucares;
    }

    public int getPor_legumbres() {
        return por_legumbres;
    }

    public void setPor_legumbres(int por_legumbres) {
        this.por_legumbres = por_legumbres;
    }

    public int getPor_frutas() {
        return por_frutas;
    }

    public void setPor_frutas(int por_frutas) {
        this.por_frutas = por_frutas;
    }

    public int getPor_cereales() {
        return por_cereales;
    }

    public void setPor_cereales(int por_cereales) {
        this.por_cereales = por_cereales;
    }

    public int getPor_grasas() {
        return por_grasas;
    }

    public void setPor_grasas(int por_grasas) {
        this.por_grasas = por_grasas;
    }
}
