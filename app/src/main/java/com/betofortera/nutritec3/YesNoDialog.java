package com.betofortera.nutritec3;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/**
 * Created by beto on 4/4/15.
 */
public class YesNoDialog extends DialogFragment {

    YesNoCommunicator communicator;
    int position;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        communicator = (YesNoCommunicator) activity;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        Bundle bundle = getArguments();
        if(bundle!=null) {
            builder.setTitle(bundle.getString("windowTitle"));
            builder.setMessage(bundle.getString("windowMessage"));
            position =  bundle.getInt("position");
        }

        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                communicator.onYesNoDialogMessage(false,position);
            }
        });
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                communicator.onYesNoDialogMessage(true,position);
            }
        });

        Dialog dialog = builder.create();
        return dialog;
    }

    interface YesNoCommunicator
    {
        public void onYesNoDialogMessage(boolean value, int position);


    }
}
