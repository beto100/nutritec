package com.betofortera.nutritec3;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

// Main Activity Class
// ////////////////////////////////////////////////////
public class MainActivity extends ActionBarActivity implements MyDialogCreate.Communicator {

    DBManager dbManager = null;
    protected Toolbar toolbar;
    protected int[] foodCounters = {0,0,0,0,0,0,0,0};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // setup to bring the user diet
        dbManager = new DBManager(this);
        SQLiteDatabase sqLiteDatabase = dbManager.getWritableDatabase();
        Diet myDiet = dbManager.getYourDiet();

        // check if user already has their die
        if (myDiet == null) {
            setContentView(R.layout.activity_main);

            // initialize the toolbar
            toolbar = (Toolbar) findViewById(R.id.app_bar);
            setSupportActionBar(toolbar);

            // add the list food fragment
            Bundle bundle = new Bundle();
            bundle.putIntArray("dynamicFoodCounter", foodCounters);
            bundle.putInt("customLay", R.layout.my_dialog_create);

            MyListFoodFragment listFoodFrag = new MyListFoodFragment();
            listFoodFrag.setArguments(bundle);
            FragmentManager manager = getFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.add(R.id.foodListFragParent, listFoodFrag, "foodFragment");
            transaction.commit();


        } else {

            // send the user to the dashboard if has diet already
            Intent intent = new Intent(this, MyDayActivity.class);
            startActivity(intent);
            finish();

        }

    }

    @Override
    public void onDialogMessage(String message, int pos) {

        if(message != null && !message.trim().equals("")) {
            try {
                foodCounters[pos] = Integer.parseInt(message.trim());
                FragmentManager manager = getFragmentManager();
                MyListFoodFragment listFragment = (MyListFoodFragment) manager.findFragmentByTag("foodFragment");
                listFragment.updateSomeRow(foodCounters);
            } catch (Exception e) {
                Toast.makeText(this,"Valor demasiado grande",Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.createSetting) {

            boolean atLeastOneCategorySetted = false;
            for(int p : foodCounters ) {
                if(p > 0) {
                    atLeastOneCategorySetted = true;
                    break;
                }
            }

            if(atLeastOneCategorySetted) {

                int vegCounter = foodCounters[0];
                int milkCounter = foodCounters[1];
                int meatCounter = foodCounters[2];
                int sugarCounter = foodCounters[3];
                int legumeCounter = foodCounters[4];
                int fruitCounter = foodCounters[5];
                int cerealCounter = foodCounters[6];
                int fatCounter = foodCounters[7];

                Diet diet = new Diet(-1,"Dieta de la Luna",TodayDate.getTodaysDate(),"2015-06-21",vegCounter,milkCounter,meatCounter,sugarCounter,legumeCounter,fruitCounter,cerealCounter,fatCounter);
                long idRes = dbManager.insertDiet(diet);
                if(idRes < 0) {
                    Toast toast = Toast.makeText(this,"Error al cargar la dieta",Toast.LENGTH_SHORT);
                    toast.show();
                }
                else {
                    Intent intent = new Intent(this, MyDayActivity.class);
                    intent.putExtra("createDietMessage","Dieta creada con éxito!");
                    startActivity(intent);
                    finish();
                }


            }
            else {
                Toast.makeText(this,"Favor de ingresar la porción de almenos una categoría",Toast.LENGTH_LONG).show();
            }
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Salida")
                .setMessage("Estas seguro de salir de la aplicación?")
                .setCancelable(false)
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        MainActivity.this.finish();
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

}

