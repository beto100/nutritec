package com.betofortera.nutritec3;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by beto on 4/20/15.
 */
public class TodayDate {

    public static String getTodaysDate()
    {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date currentTime = new Date();
        df.setTimeZone(TimeZone.getTimeZone("America/Monterrey"));
        return df.format(currentTime);
    }

    public static String getFormmatedDate(String date) {

        Date thisDate = null;
        try {
            thisDate = new SimpleDateFormat("yyyy-MM-dd").parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return new SimpleDateFormat("dd/MM/yyyy").format(thisDate);
    }
}
