package com.betofortera.nutritec3;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by beto on 4/3/15.
 */
public class MyDialogCreate extends DialogFragment {

    //protected Button cancel, ok;
    protected EditText editText;
    Communicator communicator;
    int itemPos = -1;
    int customLayout = -1;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        communicator = (Communicator) activity;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        Bundle bundle = getArguments();
        if(bundle!=null) {
            builder.setTitle(bundle.getString("foodTitle"));
            itemPos = bundle.getInt("itemPos");
            customLayout = bundle.getInt("customLay");
        }
        View view = inflater.inflate(customLayout,null);

        editText= (EditText) view.findViewById(R.id.cantidadPor);
        editText.post(new Runnable() {
            public void run() {
                editText.setFocusableInTouchMode(true);
                editText.requestFocus();
                getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).showSoftInput(editText, 0);
            }
        });

        builder.setView(view);
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                communicator.onDialogMessage(null,itemPos);
            }
        });
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                communicator.onDialogMessage(editText.getText().toString(),itemPos);
            }
        });

        Dialog dialog = builder.create();
        return dialog;
    }

    interface Communicator
    {
        public void onDialogMessage(String message, int pos);
    }

}
