package com.betofortera.nutritec3;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import it.neokree.materialtabs.MaterialTab;
import it.neokree.materialtabs.MaterialTabHost;
import it.neokree.materialtabs.MaterialTabListener;


public class ReferencesActivity extends ActionBarActivity implements MaterialTabListener {

    Toolbar toolbar;
    MaterialTabHost tabHost;
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_references);

        // set the toolbar
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        tabHost = (MaterialTabHost) findViewById(R.id.materialTabHost);
        viewPager = (ViewPager) findViewById(R.id.viewPager);


        MyPagerAdapter pagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                tabHost.setSelectedNavigationItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        for (int i = 0; i < pagerAdapter.getCount(); i++) {
            tabHost.addTab(
                    tabHost.newTab()
                            .setText(pagerAdapter.getPageTitle(i))
                            .setTabListener(this)
            );

        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_references, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }




    @Override
    public void onTabSelected(MaterialTab materialTab) {
        viewPager.setCurrentItem(materialTab.getPosition());
    }

    @Override
    public void onTabReselected(MaterialTab materialTab) {

    }

    @Override
    public void onTabUnselected(MaterialTab materialTab) {

    }


    class MyPagerAdapter extends FragmentPagerAdapter
    {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            Fragment myFragment = null;
            switch(position) {

                case 0:
                    myFragment = ReferencesListFragment.getInstance(getResources().getStringArray(R.array.vegPortions),getResources().getStringArray(R.array.vegPortionsMesaures));
                    break;
                case 1:
                    myFragment = ReferencesListFragment.getInstance(getResources().getStringArray(R.array.milkPortions),getResources().getStringArray(R.array.milkPortionsMesaures));
                    break;
                case 2:
                    myFragment = ReferencesListFragment.getInstance(getResources().getStringArray(R.array.meatPortions),getResources().getStringArray(R.array.meatPortionsMesaures));
                    break;
                case 3:
                    myFragment = ReferencesListFragment.getInstance(getResources().getStringArray(R.array.sugarPortions),getResources().getStringArray(R.array.sugarPortionsMesaures));
                    break;
                case 4:
                    myFragment = ReferencesListFragment.getInstance(getResources().getStringArray(R.array.legumesPortions),getResources().getStringArray(R.array.legumesPortionsMesaures));
                    break;
                case 5:
                    myFragment = ReferencesListFragment.getInstance(getResources().getStringArray(R.array.fruitPortions),getResources().getStringArray(R.array.fruitPortionsMesaures));
                    break;
                case 6:
                    myFragment = ReferencesListFragment.getInstance(getResources().getStringArray(R.array.cerealPortions),getResources().getStringArray(R.array.cerealPortionsMesaures));
                    break;
                case 7:
                    myFragment = ReferencesListFragment.getInstance(getResources().getStringArray(R.array.fatPortions),getResources().getStringArray(R.array.fatPortionsMesaures));
                    break;
            }

            return myFragment;
        }

        @Override
        public int getCount() {
            return 8;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return getResources().getStringArray(R.array.foodTitles)[position];
        }
    }




}
