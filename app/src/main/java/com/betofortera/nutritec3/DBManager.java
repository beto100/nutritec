package com.betofortera.nutritec3;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by beto on 3/17/15.
 */
public class DBManager extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "betofortera.db";
    private static final String TABLE_NAME = "dieta";
    private static final String TABLE_REGISTERS_NAME = "dieta_registros";
    private static final int DATABASE_VERSION = 16;

    // fields for diet table
    private static final String DIETID = "_id";
    private static final String NAME = "name";
    private static final String INIT_DATE = "init_date";
    private static final String FINAL_DATE = "final_date";
    private static final String POR_VEGETALES = "por_vegetales";
    private static final String POR_LECHE = "por_leche";
    private static final String POR_CARNES = "por_carnes";
    private static final String POR_AZUCARES = "por_azucares";
    private static final String POR_LEGUMBRES = "por_legumbres";
    private static final String POR_FRUTAS = "por_frutas";
    private static final String POR_CEREALES = "por_cereales";
    private static final String POR_GRASAS = "por_grasas";

    // fields for diet registers table (besides)
    private static final String CURRENT_DATE = "curr_date";

    private Context context;

    private static final String CREATE_DIET_TABLE = "CREATE TABLE "+ TABLE_NAME +" (" +
            "               "+ DIETID +"          INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "               "+ NAME +"            TEXT," +
            "               "+ INIT_DATE +"       TEXT," +
            "               "+ FINAL_DATE +"      TEXT," +
            "               "+ POR_VEGETALES +"   INTEGER," +
            "               "+ POR_LECHE +"       INTEGER," +
            "               "+ POR_CARNES +"      INTEGER," +
            "               "+ POR_AZUCARES +"    INTEGER," +
            "               "+ POR_LEGUMBRES +"   INTEGER," +
            "               "+ POR_FRUTAS +"      INTEGER," +
            "               "+ POR_CEREALES +"    INTEGER," +
            "               "+ POR_GRASAS +"      INTEGER" +
            "       );";


    private static final String CREATE_DIET_REGISTERS_TABLE = "CREATE TABLE "+ TABLE_REGISTERS_NAME +" (" +
            "               "+ DIETID +"            INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "               "+ CURRENT_DATE +"      TEXT," +
            "               "+ POR_VEGETALES +"     INTEGER," +
            "               "+ POR_LECHE +"         INTEGER," +
            "               "+ POR_CARNES +"        INTEGER," +
            "               "+ POR_AZUCARES +"      INTEGER," +
            "               "+ POR_LEGUMBRES +"     INTEGER," +
            "               "+ POR_FRUTAS +"        INTEGER," +
            "               "+ POR_CEREALES +"      INTEGER," +
            "               "+ POR_GRASAS +"        INTEGER" +
            "       );";

    private static final String DROP_DIET_TABLE = "DROP TABLE IF EXISTS "+ TABLE_NAME + ";";
    private static final String DROP_DIET_REGISTERS_TABLE = "DROP TABLE IF EXISTS "+ TABLE_REGISTERS_NAME + ";";


    public DBManager(Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    public Diet getYourDiet()
    {
        Diet myDiet = null;
        SQLiteDatabase db = this.getWritableDatabase();
        String[] columns = {DIETID,NAME,INIT_DATE,FINAL_DATE,POR_VEGETALES,POR_LECHE,POR_CARNES,POR_AZUCARES,POR_LEGUMBRES,POR_FRUTAS,POR_CEREALES,POR_GRASAS};
        Cursor cursor = db.query(TABLE_NAME, columns, null, null, null, null, null);
        if(cursor.moveToNext()) {

            int dId = cursor.getInt(cursor.getColumnIndex(DIETID));
            String name = cursor.getString(cursor.getColumnIndex(NAME));
            String initDate = cursor.getString(cursor.getColumnIndex(INIT_DATE));
            String finalDate = cursor.getString(cursor.getColumnIndex(FINAL_DATE));
            int por_veg = cursor.getInt(cursor.getColumnIndex(POR_VEGETALES));
            int por_milk = cursor.getInt(cursor.getColumnIndex(POR_LECHE));
            int por_meat = cursor.getInt(cursor.getColumnIndex(POR_CARNES));
            int por_sugar = cursor.getInt(cursor.getColumnIndex(POR_AZUCARES));
            int por_legumes = cursor.getInt(cursor.getColumnIndex(POR_LEGUMBRES));
            int por_fruit = cursor.getInt(cursor.getColumnIndex(POR_FRUTAS));
            int por_cereal = cursor.getInt(cursor.getColumnIndex(POR_CEREALES));
            int por_fats = cursor.getInt(cursor.getColumnIndex(POR_GRASAS));
            myDiet = new Diet(dId,name,initDate,finalDate,por_veg,por_milk,por_meat,por_sugar,por_legumes,por_fruit,por_cereal,por_fats);
        }
        db.close();
        return myDiet;
    }

    public DietRegister getTodayDietRegister(String todaysDate)
    {
        DietRegister reg = null;
        SQLiteDatabase db = this.getWritableDatabase();
        String[] columns = {DIETID,CURRENT_DATE,POR_VEGETALES,POR_LECHE,POR_CARNES,POR_AZUCARES,POR_LEGUMBRES,POR_FRUTAS,POR_CEREALES,POR_GRASAS};
        String[] whereArgs = {todaysDate};
        Cursor cursor = db.query(TABLE_REGISTERS_NAME, columns, CURRENT_DATE+" = ?", whereArgs, null, null, null);
        if(cursor.moveToNext()) {

            int id = cursor.getInt(cursor.getColumnIndex(DIETID));
            String currentDate = cursor.getString(cursor.getColumnIndex(CURRENT_DATE));
            int por_veg = cursor.getInt(cursor.getColumnIndex(POR_VEGETALES));
            int por_milk = cursor.getInt(cursor.getColumnIndex(POR_LECHE));
            int por_meat = cursor.getInt(cursor.getColumnIndex(POR_CARNES));
            int por_sugar = cursor.getInt(cursor.getColumnIndex(POR_AZUCARES));
            int por_legumes = cursor.getInt(cursor.getColumnIndex(POR_LEGUMBRES));
            int por_fruit = cursor.getInt(cursor.getColumnIndex(POR_FRUTAS));
            int por_cereal = cursor.getInt(cursor.getColumnIndex(POR_CEREALES));
            int por_fats = cursor.getInt(cursor.getColumnIndex(POR_GRASAS));
            reg = new DietRegister(id, currentDate, por_veg, por_milk, por_meat, por_sugar, por_legumes, por_fruit, por_cereal, por_fats);
        }
        return reg;
    }

    public DietRegister getDietRegisterById(int regId)
    {
        DietRegister reg = null;
        SQLiteDatabase db = this.getWritableDatabase();
        String[] columns = {DIETID,CURRENT_DATE,POR_VEGETALES,POR_LECHE,POR_CARNES,POR_AZUCARES,POR_LEGUMBRES,POR_FRUTAS,POR_CEREALES,POR_GRASAS};
        String[] whereArgs = {String.valueOf(regId)};
        Cursor cursor = db.query(TABLE_REGISTERS_NAME, columns, DIETID+" = ?",whereArgs, null, null, null);
        if(cursor.moveToNext()) {

            int id = cursor.getInt(cursor.getColumnIndex(DIETID));
            String currentDate = cursor.getString(cursor.getColumnIndex(CURRENT_DATE));
            int por_veg = cursor.getInt(cursor.getColumnIndex(POR_VEGETALES));
            int por_milk = cursor.getInt(cursor.getColumnIndex(POR_LECHE));
            int por_meat = cursor.getInt(cursor.getColumnIndex(POR_CARNES));
            int por_sugar = cursor.getInt(cursor.getColumnIndex(POR_AZUCARES));
            int por_legumes = cursor.getInt(cursor.getColumnIndex(POR_LEGUMBRES));
            int por_fruit = cursor.getInt(cursor.getColumnIndex(POR_FRUTAS));
            int por_cereal = cursor.getInt(cursor.getColumnIndex(POR_CEREALES));
            int por_fats = cursor.getInt(cursor.getColumnIndex(POR_GRASAS));
            reg = new DietRegister(id, currentDate, por_veg, por_milk, por_meat, por_sugar, por_legumes, por_fruit, por_cereal, por_fats);
        }
        cursor.close();
        return reg;
    }

    public ArrayList<DietRegister> getFirstAndLastDietRegisters()
    {
        ArrayList<DietRegister> list = new ArrayList<DietRegister>();
        SQLiteDatabase db = this.getWritableDatabase();
        String[] columns = {DIETID,CURRENT_DATE};
        Cursor cursor = db.query(TABLE_REGISTERS_NAME, columns, null, null, null, null,CURRENT_DATE);

        if(cursor.getCount() > 0) {
            cursor.moveToFirst();
            int id = cursor.getInt(cursor.getColumnIndex(DIETID));
            String currentDate = cursor.getString(cursor.getColumnIndex(CURRENT_DATE));
            DietRegister firstReg = new DietRegister(id, currentDate, 0, 0, 0, 0, 0, 0, 0, 0);
            list.add(firstReg);

            cursor.moveToLast();
            id = cursor.getInt(cursor.getColumnIndex(DIETID));
            currentDate = cursor.getString(cursor.getColumnIndex(CURRENT_DATE));
            DietRegister lastReg = new DietRegister(id, currentDate, 0, 0, 0, 0, 0, 0, 0, 0);
            list.add(lastReg);
        }

        return list;
    }

    public ArrayList<DietRegister> getAllDietRegisters()
    {
        ArrayList<DietRegister> list = new ArrayList<DietRegister>();
        SQLiteDatabase db = this.getWritableDatabase();
        String[] columns = {DIETID,CURRENT_DATE};
        Cursor cursor = db.query(TABLE_REGISTERS_NAME, columns, null, null, null, null, null);
        while(cursor.moveToNext()) {

            int id = cursor.getInt(cursor.getColumnIndex(DIETID));
            String currentDate = cursor.getString(cursor.getColumnIndex(CURRENT_DATE));
            int por_veg = 0;
            int por_milk = 0;
            int por_meat = 0;
            int por_sugar = 0;
            int por_legumes = 0;
            int por_fruit = 0;
            int por_cereal = 0;
            int por_fats = 0;
            DietRegister newReg = new DietRegister(id, currentDate, por_veg, por_milk, por_meat, por_sugar, por_legumes, por_fruit, por_cereal, por_fats);
            list.add(newReg);
        }
        return list;
    }

    public ArrayList<DietRegister> getDietRegistersBetweenDates(String lowerDate, String superiorDate)
    {
        ArrayList<DietRegister> list = new ArrayList<DietRegister>();
        SQLiteDatabase db = this.getWritableDatabase();
        String[] columns = {DIETID,CURRENT_DATE,POR_VEGETALES,POR_LECHE,POR_CARNES,POR_AZUCARES,POR_LEGUMBRES,POR_FRUTAS,POR_CEREALES,POR_GRASAS};
        String[] whereArgs = {lowerDate,superiorDate};
        Cursor cursor = db.query(TABLE_REGISTERS_NAME, columns, CURRENT_DATE + " BETWEEN ? AND ?", whereArgs, null, null, null);
        while(cursor.moveToNext()) {

            int id = cursor.getInt(cursor.getColumnIndex(DIETID));
            String currentDate = cursor.getString(cursor.getColumnIndex(CURRENT_DATE));
            int por_veg = cursor.getInt(cursor.getColumnIndex(POR_VEGETALES));
            int por_milk = cursor.getInt(cursor.getColumnIndex(POR_LECHE));
            int por_meat = cursor.getInt(cursor.getColumnIndex(POR_CARNES));
            int por_sugar = cursor.getInt(cursor.getColumnIndex(POR_AZUCARES));
            int por_legumes = cursor.getInt(cursor.getColumnIndex(POR_LEGUMBRES));
            int por_fruit = cursor.getInt(cursor.getColumnIndex(POR_FRUTAS));
            int por_cereal = cursor.getInt(cursor.getColumnIndex(POR_CEREALES));
            int por_fats = cursor.getInt(cursor.getColumnIndex(POR_GRASAS));
            DietRegister newReg = new DietRegister(id, currentDate, por_veg, por_milk, por_meat, por_sugar, por_legumes, por_fruit, por_cereal, por_fats);
            list.add(newReg);
        }
        return list;
    }

    public ArrayList<MonthYear> getDifferentMonthsYears()
    {
        ArrayList<MonthYear> list = new ArrayList<MonthYear>();
        SQLiteDatabase db = this.getWritableDatabase();
        String[] columns = {"strftime('%m',"+ CURRENT_DATE + ") as 'Month'", "strftime('%Y',"+ CURRENT_DATE + ") as 'Year'"};
        Cursor cursor = db.query(true,TABLE_REGISTERS_NAME, columns, null, null, null, null, null,null);
        while(cursor.moveToNext()) {
            String newMonth = cursor.getString(cursor.getColumnIndex("Month"));
            String newYear = cursor.getString(cursor.getColumnIndex("Year"));
            MonthYear newReg = new MonthYear(newMonth,newYear);
            list.add(newReg);
        }
        return list;
    }

    public ArrayList<DietRegister> getDietRegistersByMonthYear(String month, String year)
    {
        ArrayList<DietRegister> list = new ArrayList<DietRegister>();
        SQLiteDatabase db = this.getWritableDatabase();
        String[] columns = {DIETID,CURRENT_DATE,POR_VEGETALES,POR_LECHE,POR_CARNES,POR_AZUCARES,POR_LEGUMBRES,POR_FRUTAS,POR_CEREALES,POR_GRASAS};
        String[] whereArgs = {month,year};
        Cursor cursor = db.query(TABLE_REGISTERS_NAME, columns, "strftime('%m',"+ CURRENT_DATE +") = ? AND strftime('%Y',"+ CURRENT_DATE +") = ?", whereArgs, null, null, null);
        while(cursor.moveToNext()) {

            int id = cursor.getInt(cursor.getColumnIndex(DIETID));
            String currentDate = cursor.getString(cursor.getColumnIndex(CURRENT_DATE));
            int por_veg = cursor.getInt(cursor.getColumnIndex(POR_VEGETALES));
            int por_milk = cursor.getInt(cursor.getColumnIndex(POR_LECHE));
            int por_meat = cursor.getInt(cursor.getColumnIndex(POR_CARNES));
            int por_sugar = cursor.getInt(cursor.getColumnIndex(POR_AZUCARES));
            int por_legumes = cursor.getInt(cursor.getColumnIndex(POR_LEGUMBRES));
            int por_fruit = cursor.getInt(cursor.getColumnIndex(POR_FRUTAS));
            int por_cereal = cursor.getInt(cursor.getColumnIndex(POR_CEREALES));
            int por_fats = cursor.getInt(cursor.getColumnIndex(POR_GRASAS));
            DietRegister newReg = new DietRegister(id, currentDate, por_veg, por_milk, por_meat, por_sugar, por_legumes, por_fruit, por_cereal, por_fats);
            list.add(newReg);
        }
        return list;
    }

    public long insertDiet(Diet diet)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(NAME,diet.getNombre());
        contentValues.put(INIT_DATE, diet.getInitDate());
        contentValues.put(FINAL_DATE, diet.getFinalDate());
        contentValues.put(POR_VEGETALES, diet.getPor_vegetales());
        contentValues.put(POR_LECHE, diet.getPor_leche());
        contentValues.put(POR_CARNES, diet.getPor_carnes());
        contentValues.put(POR_AZUCARES, diet.getPor_azucares());
        contentValues.put(POR_LEGUMBRES, diet.getPor_legumbres());
        contentValues.put(POR_FRUTAS, diet.getPor_frutas());
        contentValues.put(POR_CEREALES, diet.getPor_cereales());
        contentValues.put(POR_GRASAS, diet.getPor_grasas());
        long id = db.insert(TABLE_NAME,null,contentValues);
        return id;
    }

    public long insertDietRegister(DietRegister dr)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CURRENT_DATE, dr.getCurrDate());
        contentValues.put(POR_VEGETALES, dr.getPor_vegetales());
        contentValues.put(POR_LECHE, dr.getPor_leche());
        contentValues.put(POR_CARNES, dr.getPor_carnes());
        contentValues.put(POR_AZUCARES, dr.getPor_azucares());
        contentValues.put(POR_LEGUMBRES, dr.getPor_legumbres());
        contentValues.put(POR_FRUTAS, dr.getPor_frutas());
        contentValues.put(POR_CEREALES, dr.getPor_cereales());
        contentValues.put(POR_GRASAS, dr.getPor_grasas());
        long id = db.insert(TABLE_REGISTERS_NAME,null,contentValues);
        return id;
    }

    public int updateDietRegister(DietRegister dr)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(POR_VEGETALES, dr.getPor_vegetales());
        contentValues.put(POR_LECHE, dr.getPor_leche());
        contentValues.put(POR_CARNES, dr.getPor_carnes());
        contentValues.put(POR_AZUCARES, dr.getPor_azucares());
        contentValues.put(POR_LEGUMBRES, dr.getPor_legumbres());
        contentValues.put(POR_FRUTAS, dr.getPor_frutas());
        contentValues.put(POR_CEREALES, dr.getPor_cereales());
        contentValues.put(POR_GRASAS, dr.getPor_grasas());
        String[] whereArgs = {dr.getCurrDate()};
        int count = db.update(TABLE_REGISTERS_NAME,contentValues,CURRENT_DATE+" = ?",whereArgs);
        return count;
    }

    public void deleteDiet()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, null,null);
        db.delete(TABLE_REGISTERS_NAME, null,null); // delete registers
        db.close();
    }

    public int updateDiet(Diet diet)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(NAME,diet.getNombre());
        contentValues.put(POR_VEGETALES, diet.getPor_vegetales());
        contentValues.put(POR_LECHE, diet.getPor_leche());
        contentValues.put(POR_CARNES, diet.getPor_carnes());
        contentValues.put(POR_AZUCARES, diet.getPor_azucares());
        contentValues.put(POR_LEGUMBRES, diet.getPor_legumbres());
        contentValues.put(POR_FRUTAS, diet.getPor_frutas());
        contentValues.put(POR_CEREALES, diet.getPor_cereales());
        contentValues.put(POR_GRASAS, diet.getPor_grasas());
        int count = db.update(TABLE_NAME,contentValues,null,null);
        db.close();
        return count;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            //Toast.makeText(this.context,"onCreate called",Toast.LENGTH_SHORT).show();
            db.execSQL(CREATE_DIET_TABLE);
            db.execSQL(CREATE_DIET_REGISTERS_TABLE);
        }
        catch(Exception ex) {

            Toast.makeText(this.context,"Fail to create database",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            //Toast.makeText(this.context,"onUpgrade called",Toast.LENGTH_SHORT).show();
            db.execSQL(DROP_DIET_TABLE);
            db.execSQL(DROP_DIET_REGISTERS_TABLE);
            onCreate(db);
        }
        catch(Exception ex) {

            Toast.makeText(this.context,"Fail to update database",Toast.LENGTH_SHORT).show();
        }
    }
}
