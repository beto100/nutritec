package com.betofortera.nutritec3;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v4.app.ListFragment;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by beto on 4/19/15.
 */
public class RecordsListFragment extends ListFragment implements AdapterView.OnItemClickListener {

    protected TimeRegistersAdapter timeRegAdapter =null;
    protected int[] _dayIds = null;
    protected String[] timeRecordTitles = null;

    // verifiers for existence
    ArrayList<DietRegister> listOfDays = null;
    protected ArrayList<BetweenDates> betDatesList = null;
    ArrayList<MonthYear> monthsList = null;

    DBManager dbManager = null;
    int whichTab = -1;

    private String getStringWeekDay(int dayId) {
        String stringDay = null;
        switch (dayId) {
            case 1:
                stringDay = "DOMINGO";
                break;
            case 2:
                stringDay = "LUNES";
                break;
            case 3:
                stringDay = "MARTES";
                break;
            case 4:
                stringDay = "MIERCOLES";
                break;
            case 5:
                stringDay = "JUEVES";
                break;
            case 6:
                stringDay = "VIERNES";
                break;
            case 7:
                stringDay = "SABADO";
                break;
        }
        return stringDay;
    }

    private String getMonthName(String monthId) {
        String monthName = null;
        switch(monthId) {
            case "01":
                monthName = "ENERO";
                break;
            case "02":
                monthName = "FEBRERO";
                break;
            case "03":
                monthName = "MARZO";
                break;
            case "04":
                monthName = "ABRIL";
                break;
            case "05":
                monthName = "MAYO";
                break;
            case "06":
                monthName = "JUNIO";
                break;
            case "07":
                monthName = "JULIO";
                break;
            case "08":
                monthName = "AGOSTO";
                break;
            case "09":
                monthName = "SEPTIEMBRE";
                break;
            case "10":
                monthName = "OCTUBRE";
                break;
            case "11":
                monthName = "NOVIEMBRE";
                break;
            case "12":
                monthName = "DICIEMBRE";
                break;
        }

        return monthName;
    }

    public static RecordsListFragment getInstance(int whichTab)
    {
        RecordsListFragment rlf = new RecordsListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("cual",whichTab);
        rlf.setArguments(bundle);
        return rlf;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        Bundle bundle = getArguments();
        if(bundle!=null) {
            this.whichTab = bundle.getInt("cual");
        }

        betDatesList = new ArrayList<BetweenDates>();
        dbManager = new DBManager(getActivity());
        SQLiteDatabase sqLiteDatabase = dbManager.getWritableDatabase();
        ArrayAdapter<String> adapter = null;

        if(whichTab == 0) {

            listOfDays = dbManager.getAllDietRegisters();
            int sizeOfListOfDays = listOfDays.size();
            if(sizeOfListOfDays > 0) {
                _dayIds = new int[sizeOfListOfDays];
                timeRecordTitles = new String[sizeOfListOfDays];

                for (int i = 0; i < sizeOfListOfDays; i++) {

                    // fill reg ids
                    _dayIds[i] = listOfDays.get(i).getId();

                    // fill string titles
                    String thisDate = listOfDays.get(i).getCurrDate();
                    Calendar c = Calendar.getInstance();
                    Date date = null;
                    try {
                        date = new SimpleDateFormat("yyyy-MM-dd").parse(thisDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    c.setTime(date);
                    int intDayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                    String stringDayOfWeek = getStringWeekDay(intDayOfWeek);
                    timeRecordTitles[i] = stringDayOfWeek + " - " + TodayDate.getFormmatedDate(thisDate);
                }
            }
            else {
                timeRecordTitles = new String[1];
                timeRecordTitles[0] = "No hay registros";
            }
        }
        else if(whichTab == 1) {

            ArrayList<DietRegister> firstLastList = dbManager.getFirstAndLastDietRegisters();
            boolean enterAtLeastOnce = false;

            if(firstLastList.size() > 0) {
                String firstRecord = firstLastList.get(0).getCurrDate();
                String lastRecord = firstLastList.get(1).getCurrDate();
                Date dateFirstRecord = null;
                Date dateLastRecord = null;
                try {
                    dateFirstRecord = new SimpleDateFormat("yyyy-MM-dd").parse(firstRecord);
                    dateLastRecord = new SimpleDateFormat("yyyy-MM-dd").parse(lastRecord);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

                Calendar c = Calendar.getInstance();
                c.setTime(dateFirstRecord);
                while (c.getTime().before(dateLastRecord) || c.getTime().equals(dateLastRecord)) {
                    enterAtLeastOnce = true;

                    c.add(Calendar.DATE, 6);
                    betDatesList.add(new BetweenDates(df.format(dateFirstRecord), df.format(c.getTime())));

                    // add 1 day after adding to the list to the dateFirstRecord
                    Calendar cTemp = Calendar.getInstance();
                    cTemp.setTime(dateFirstRecord);
                    cTemp.add(Calendar.DATE, 7);
                    dateFirstRecord = cTemp.getTime();

                    // add another 6 days to the current dateFirstRecord
                    c.setTime(dateFirstRecord);
                }
            }

            // check if the control enters at least once in the while loop
            if(enterAtLeastOnce) {
                timeRecordTitles = new String[betDatesList.size()];
                for(int i = 0; i < betDatesList.size(); i++) {
                    timeRecordTitles[i] = TodayDate.getFormmatedDate(betDatesList.get(i).lowerDate) + " - " + TodayDate.getFormmatedDate(betDatesList.get(i).superiorDate);
                }
            }
            else {
                timeRecordTitles = new String[1];
                timeRecordTitles[0] = "No hay registros";
            }
        }
        else if(whichTab == 2) {

            monthsList = dbManager.getDifferentMonthsYears();
            if(monthsList.size() > 0) {
                timeRecordTitles = new String[monthsList.size()];
                for(int i = 0; i < monthsList.size(); i++) {
                    timeRecordTitles[i] = getMonthName(monthsList.get(i).month) +" - "+ monthsList.get(i).year;
                }
            }
            else {
                timeRecordTitles = new String[1];
                timeRecordTitles[0] = "No hay registros";
            }
        }

        timeRegAdapter = new TimeRegistersAdapter(getActivity(),this.whichTab,this.timeRecordTitles,this._dayIds,this.betDatesList,this.monthsList);
        setListAdapter(timeRegAdapter);
        getListView().setOnItemClickListener(this);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Intent intent = null;
        if(whichTab == 0) {

            if(listOfDays.size() > 0) {
                intent = new Intent(getActivity(), RecordDetail.class);
                intent.putExtra("whichTab", whichTab);
                intent.putExtra("dbIdRecord", _dayIds[position]);
            }
        }
        else if(whichTab == 1) {

            if(betDatesList.size() > 0) {
                intent = new Intent(getActivity(), RecordDetail.class);
                intent.putExtra("whichTab", whichTab);
                intent.putExtra("lowerDate", betDatesList.get(position).lowerDate);
                intent.putExtra("superiorDate",betDatesList.get(position).superiorDate);
            }
        }
        else if(whichTab == 2) {
            if(monthsList.size() > 0) {
                intent = new Intent(getActivity(), RecordDetail.class);
                intent.putExtra("whichTab", whichTab);
                intent.putExtra("month", monthsList.get(position).month);
                intent.putExtra("year",monthsList.get(position).year);
            }
        }

        TextView rowText = (TextView) view.findViewById(R.id.time_title);
        intent.putExtra("listTitle", rowText.getText());
        startActivity(intent);
    }

}

class BetweenDates {

    String lowerDate;
    String superiorDate;

    BetweenDates(String lowerDate, String superiorDate) {
        this.lowerDate = lowerDate;
        this.superiorDate = superiorDate;
    }
}

class MonthYear
{
    public String month;
    public String year;

    MonthYear(String month, String year) {
        this.month = month;
        this.year = year;
    }
}


// TimeRegistersAdapter
// ////////////////////////////////////////////////////
class TimeRegistersAdapter extends BaseAdapter
{
    Context context;
    int whichTab = -1;
    int[] arrayOfDays = null;
    ArrayList<BetweenDates> betDatesList = null;
    ArrayList<MonthYear> monthsList = null;
    String[] timeRecordTitles = null;
    DBManager db;


    public TimeRegistersAdapter(Context c, int whichTab, String[] timeRecordTitles, int[] listOfDays, ArrayList<BetweenDates> betDatesList,ArrayList<MonthYear> monthsList)
    {
        context = c;
        this.whichTab = whichTab;
        this.arrayOfDays = listOfDays;
        this.betDatesList = betDatesList;
        this.monthsList = monthsList;
        this.timeRecordTitles = timeRecordTitles;
        db = new DBManager(c);
    }

    @Override
    public int getCount() {
        int sizeOfList = -1;
        switch(this.whichTab)
        {
            case 0:
                sizeOfList = this.arrayOfDays.length;
                break;
            case 1:
                sizeOfList = this.betDatesList.size();
                break;
            case 2:
                sizeOfList = this.monthsList.size();
                break;
        }

        return sizeOfList;
    }

    @Override
    public Object getItem(int position) {
        Object item = null;
        switch(this.whichTab)
        {
            case 0:
                item = this.arrayOfDays[position];
                break;
            case 1:
                item = this.betDatesList.get(position);
                break;
            case 2:
                item = this.monthsList.get(position);
                break;
        }

        return item;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    class MyViewHolder
    {
        ImageView doneFailImage;
        TextView timeTitle;

        MyViewHolder(View v) {
            this.doneFailImage = (ImageView) v.findViewById(R.id.doneFail_image);
            this.timeTitle = (TextView) v.findViewById(R.id.time_title);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;
        MyViewHolder holder = null;
        if(row == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.row_record, parent, false);
            holder = new MyViewHolder(row);
            row.setTag(holder);
        }
        else {
            holder = (MyViewHolder) row.getTag();
        }

        Diet myDiet = db.getYourDiet();
        boolean done = true;

        if(this.whichTab == 0) {

            int thisDay = this.arrayOfDays[position];
            DietRegister dr = db.getDietRegisterById(thisDay);
            if(dr.getPor_vegetales() < myDiet.getPor_vegetales()) done = false;
            if(dr.getPor_leche() < myDiet.getPor_leche()) done = false;
            if(dr.getPor_carnes() < myDiet.getPor_carnes()) done = false;
            if(dr.getPor_azucares() < myDiet.getPor_azucares()) done = false;
            if(dr.getPor_legumbres() < myDiet.getPor_legumbres()) done = false;
            if(dr.getPor_frutas() < myDiet.getPor_frutas()) done = false;
            if(dr.getPor_cereales() < myDiet.getPor_cereales()) done = false;
            if(dr.getPor_grasas() < myDiet.getPor_grasas()) done = false;

        }
        else if(this.whichTab == 1 || this.whichTab == 2) {

            ArrayList<DietRegister> drList = null;
            if(this.whichTab == 1) {

                BetweenDates betDates = this.betDatesList.get(position);
                drList = db.getDietRegistersBetweenDates(betDates.lowerDate,betDates.superiorDate);
            }
            else {

                MonthYear my = this.monthsList.get(position);
                drList = db.getDietRegistersByMonthYear(my.month,my.year);
            }

            int summVeg = 0, summLeche = 0, summCarnes = 0, summAzuc = 0, summLegum = 0, summFrutas = 0, summCer = 0, summGrasas = 0;
            for(DietRegister dr : drList) {
                summVeg += dr.getPor_vegetales();
                summLeche += dr.getPor_leche();
                summCarnes += dr.getPor_carnes();
                summAzuc += dr.getPor_azucares();
                summLegum += dr.getPor_legumbres();
                summFrutas += dr.getPor_frutas();
                summCer += dr.getPor_cereales();
                summGrasas += dr.getPor_grasas();
            }

            if(summVeg < myDiet.getPor_vegetales() * drList.size()) done = false;
            if(summLeche < myDiet.getPor_leche() * drList.size()) done = false;
            if(summCarnes < myDiet.getPor_carnes() * drList.size()) done = false;
            if(summAzuc < myDiet.getPor_azucares() * drList.size()) done = false;
            if(summLegum < myDiet.getPor_legumbres() * drList.size()) done = false;
            if(summFrutas < myDiet.getPor_frutas() * drList.size()) done = false;
            if(summCer < myDiet.getPor_cereales() * drList.size()) done = false;
            if(summGrasas < myDiet.getPor_grasas() * drList.size()) done = false;


        }

        if(done) {
            holder.doneFailImage.setImageResource(R.drawable.check);
        }
        else {
            holder.doneFailImage.setImageResource(R.drawable.close);
        }
        holder.timeTitle.setText(this.timeRecordTitles[position]);
        return row;
    }
}


