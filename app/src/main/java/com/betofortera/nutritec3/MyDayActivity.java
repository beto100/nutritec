package com.betofortera.nutritec3;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.NavUtils;
import android.support.v4.content.IntentCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;


public class MyDayActivity extends ActionBarActivity implements MyDialogCreate.Communicator, YesNoDialog.YesNoCommunicator {

    protected int[] maxFoodCounters = null;
    protected int[] registersFoodCounters = null;
    DBManager dbManager = null;
    TextView todaysDateText;
    DrawerLayout dl;
    public static boolean navOpen = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_day);

        Bundle createDietbundle = getIntent().getExtras();
        if(createDietbundle != null) {
            Toast.makeText(this,createDietbundle.getString("createDietMessage"),Toast.LENGTH_LONG).show();
        }

        // set the toolbar
        // ===============================================================
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        NavigationDrawerFragment drawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragment_nav_drawer);

        dl = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerFragment.setUp(R.id.fragment_nav_drawer,dl,toolbar,this);


        boolean hasDietRegister = true;
        // add the list fragment
        // ===============================================================
        dbManager = new DBManager(this);
        SQLiteDatabase sqLiteDatabase = dbManager.getWritableDatabase();

        maxFoodCounters = new int[8];
        registersFoodCounters = new int[8];

        Diet myDiet = dbManager.getYourDiet();
        maxFoodCounters[0] = myDiet.getPor_vegetales();
        maxFoodCounters[1] = myDiet.getPor_leche();
        maxFoodCounters[2] = myDiet.getPor_carnes();
        maxFoodCounters[3] = myDiet.getPor_azucares();
        maxFoodCounters[4] = myDiet.getPor_legumbres();
        maxFoodCounters[5] = myDiet.getPor_frutas();
        maxFoodCounters[6] = myDiet.getPor_cereales();
        maxFoodCounters[7] = myDiet.getPor_grasas();

        String todaysDate = TodayDate.getTodaysDate();
        DietRegister myDietReg = dbManager.getTodayDietRegister(todaysDate);
        if(myDietReg == null) {
            // user does not have todays diet register
            hasDietRegister = false;
            DietRegister newReg = new DietRegister(-1,todaysDate,0,0,0,0,0,0,0,0);
            dbManager.insertDietRegister(newReg);
        }

        if(!hasDietRegister) {
            myDietReg = dbManager.getTodayDietRegister(todaysDate);
        }

        // set formatted date
        todaysDateText = (TextView) findViewById(R.id.todaysDate);
        todaysDateText.setText("Hoy: " + TodayDate.getFormmatedDate(myDietReg.getCurrDate()));

        // fill up the registersFoodCounter array
        registersFoodCounters[0] = myDietReg.getPor_vegetales();
        registersFoodCounters[1] = myDietReg.getPor_leche();
        registersFoodCounters[2] = myDietReg.getPor_carnes();
        registersFoodCounters[3] = myDietReg.getPor_azucares();
        registersFoodCounters[4] = myDietReg.getPor_legumbres();
        registersFoodCounters[5] = myDietReg.getPor_frutas();
        registersFoodCounters[6] = myDietReg.getPor_cereales();
        registersFoodCounters[7] = myDietReg.getPor_grasas();

        Bundle bundle = new Bundle();
        bundle.putIntArray("dynamicMaxFoodCounter", maxFoodCounters);
        bundle.putIntArray("dynamicRegistersCounters",registersFoodCounters);
        bundle.putInt("customLay", R.layout.dialog_my_day);

        MyDayListFoodFragment listFoodFrag = new MyDayListFoodFragment();
        listFoodFrag.setArguments(bundle);
        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.foodListFragParentMyDay, listFoodFrag, "foodFragmentMyDay");
        transaction.commit();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my_day, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.settingRegisterChanges) {

            String todaysDate = TodayDate.getTodaysDate();
            DietRegister upDr = new DietRegister(-1,todaysDate,registersFoodCounters[0],registersFoodCounters[1],registersFoodCounters[2],registersFoodCounters[3],registersFoodCounters[4],registersFoodCounters[5],registersFoodCounters[6],registersFoodCounters[7]);
            int affected_rows = dbManager.updateDietRegister(upDr);
            if(affected_rows <= 0) {
                Toast.makeText(this,"Lo sentimos pero ya no es posible que registres mas cambios, ya que se ha pasado al día siguiente",Toast.LENGTH_LONG).show();
            }
            else {
                Toast.makeText(this,"Los cambios se han registrado con éxito!",Toast.LENGTH_LONG).show();
            }
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onDialogMessage(String message, int pos) {

        if(message != null && !message.trim().equals("")) {
            try {
            int numToAdd = Integer.parseInt(message.trim());

            if(registersFoodCounters[pos] + numToAdd > maxFoodCounters[pos]) {
                Toast.makeText(this,"Has sobrepasado la cantidad de porciones de "+ getResources().getStringArray(R.array.foodTitles)[pos] +" en este día",Toast.LENGTH_SHORT).show();
               // return;
            }

            registersFoodCounters[pos] = registersFoodCounters[pos] + numToAdd;
            FragmentManager manager = getFragmentManager();
            MyDayListFoodFragment listFragment = (MyDayListFoodFragment) manager.findFragmentByTag("foodFragmentMyDay");
            listFragment.updateRegisterCounterRow(registersFoodCounters);
            } catch (Exception e) {
                Toast.makeText(this,"Valor demasiado grande",Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onYesNoDialogMessage(boolean value, int position) {

        if(value) {

            registersFoodCounters[position]=0;
            FragmentManager manager = getFragmentManager();
            MyDayListFoodFragment listFragment = (MyDayListFoodFragment) manager.findFragmentByTag("foodFragmentMyDay");
            listFragment.updateRegisterCounterRow(registersFoodCounters);
        }

    }

    @Override
    public void onBackPressed() {

        if(navOpen) {
            if (dl.isDrawerOpen(Gravity.LEFT)) {
                dl.closeDrawer(Gravity.LEFT);
            } else {
                super.onBackPressed();
            }
        }
        else {
            new AlertDialog.Builder(this)
                    .setTitle("Salida")
                    .setMessage("Estas seguro de salir de la aplicación?")
                    .setCancelable(false)
                    .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            MyDayActivity.this.finish();
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();
        }
    }
}
