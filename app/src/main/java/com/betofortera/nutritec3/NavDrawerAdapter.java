package com.betofortera.nutritec3;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by beto on 5/18/15.
 */
public class NavDrawerAdapter extends RecyclerView.Adapter<NavDrawerAdapter.MyViewHolder> {

    private final LayoutInflater inflater;
    List<NavInfoRow> data = Collections.emptyList();
    Context context;


    public NavDrawerAdapter(Context c, List<NavInfoRow> data)
    {
        context = c;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view=inflater.inflate(R.layout.row_drawer,parent,false);
        MyViewHolder holder = new MyViewHolder(view,this.context);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder viewHolder, int i) {

        NavInfoRow current = data.get(i);
        viewHolder.title.setText(current.title);
        viewHolder.icon.setImageResource(current.iconId);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView title;
        ImageView icon;
        Context context;

        public MyViewHolder(View itemView,Context c) {
            super(itemView);
            context = c;
            itemView.setOnClickListener(this);
            title = (TextView) itemView.findViewById(R.id.list_text);
            icon = (ImageView) itemView.findViewById(R.id.list_icon);
        }

        @Override
        public void onClick(View v) {
            TextView textView = (TextView)v.findViewById(R.id.list_text);
            String titleView = textView.getText().toString();

            Intent intent = null;
            if(titleView.equals("Mi Dieta")) {
                intent = new Intent(context, MyDietActivity.class);
            }
            else if(titleView.equals("Mi Historial")) {
                intent = new Intent(context, RecordsActivity.class);
            }
            else if(titleView.equals("Mis Referencias")) {
                intent = new Intent(context, ReferencesActivity.class);
            }
            else {
                intent = new Intent(context, AboutActivity.class);
            }

            context.startActivity(intent);
        }
    }
}
