package com.betofortera.nutritec3;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;


public class RecordDetail extends ActionBarActivity {

    int whichTab = 0;
    int regDayId = 0;
    int[] percentageArray = null;
    DBManager dbManager = null;
    TextView listTitle;
    Diet myDiet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record_detail);

        // set the toolbar
        listTitle = (TextView) findViewById(R.id.recordDetailTitle);
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        // set the list fragment
        percentageArray = new int[8];
        dbManager = new DBManager(this);
        SQLiteDatabase sqLiteDatabase = dbManager.getWritableDatabase();

        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            whichTab = bundle.getInt("whichTab");
            listTitle.setText(bundle.getString("listTitle"));
        }
        myDiet = dbManager.getYourDiet();

        if(whichTab == 0) {
            regDayId = bundle.getInt("dbIdRecord");
            DietRegister myDietReg = dbManager.getDietRegisterById(regDayId);

            if(myDiet.getPor_vegetales() == 0) {
                percentageArray[0] = (int) (((double) myDietReg.getPor_vegetales() / 1.0) * 100);
            }
            else {
                percentageArray[0] = (int) (((double) myDietReg.getPor_vegetales() / (double) myDiet.getPor_vegetales()) * 100);
            }

            if(myDiet.getPor_leche() == 0) {
                percentageArray[1] = (int) (((double) myDietReg.getPor_leche() / 1.0) * 100);
            }
            else {
                percentageArray[1] = (int) (((double) myDietReg.getPor_leche() / (double) myDiet.getPor_leche()) * 100);
            }

            if(myDiet.getPor_carnes() == 0) {
                percentageArray[2] = (int) (((double) myDietReg.getPor_carnes() / 1.0) * 100);
            }
            else {
                percentageArray[2] = (int) (((double) myDietReg.getPor_carnes() / (double) myDiet.getPor_carnes()) * 100);
            }

            if(myDiet.getPor_azucares() == 0) {
                percentageArray[3] = (int) (((double) myDietReg.getPor_azucares() / 1.0) * 100);
            }
            else {
                percentageArray[3] = (int) (((double) myDietReg.getPor_azucares() / (double) myDiet.getPor_azucares()) * 100);
            }

            if(myDiet.getPor_legumbres() == 0) {
                percentageArray[4] = (int) (((double) myDietReg.getPor_legumbres() / 1.0) * 100);
            }
            else {
                percentageArray[4] = (int) (((double) myDietReg.getPor_legumbres() / (double) myDiet.getPor_legumbres()) * 100);
            }

            if(myDiet.getPor_frutas() == 0) {
                percentageArray[5] = (int) (((double) myDietReg.getPor_frutas() / 1.0) * 100);
            }
            else {
                percentageArray[5] = (int) (((double) myDietReg.getPor_frutas() / (double) myDiet.getPor_frutas()) * 100);
            }

            if(myDiet.getPor_cereales() == 0) {
                percentageArray[6] = (int) (((double) myDietReg.getPor_cereales() / 1.0) * 100);
            }
            else {
                percentageArray[6] = (int) (((double) myDietReg.getPor_cereales() / (double) myDiet.getPor_cereales()) * 100);
            }

            if(myDiet.getPor_grasas() == 0) {
                percentageArray[7] = (int) (((double) myDietReg.getPor_grasas() / 1.0) * 100);
            }
            else {
                percentageArray[7] = (int) (((double) myDietReg.getPor_grasas() / (double) myDiet.getPor_grasas()) * 100);
            }
        }
        else if(whichTab == 1 || whichTab == 2) {
            ArrayList<DietRegister> dietReglist = null;

            if(whichTab == 1) {
                String lowerDate = bundle.getString("lowerDate");
                String superiorDate = bundle.getString("superiorDate");
                dietReglist = dbManager.getDietRegistersBetweenDates(lowerDate, superiorDate);
            }
            else if(whichTab == 2) {
                String month = bundle.getString("month");
                String year = bundle.getString("year");
                dietReglist = dbManager.getDietRegistersByMonthYear(month,year);
            }

            int summVeg = 0, summLeche = 0, summCarnes = 0, summAzuc = 0, summLegum = 0, summFrutas = 0, summCer = 0, summGrasas = 0;
            for(DietRegister dr : dietReglist) {
                summVeg += dr.getPor_vegetales();
                summLeche += dr.getPor_leche();
                summCarnes += dr.getPor_carnes();
                summAzuc += dr.getPor_azucares();
                summLegum += dr.getPor_legumbres();
                summFrutas += dr.getPor_frutas();
                summCer += dr.getPor_cereales();
                summGrasas += dr.getPor_grasas();
            }

            if(myDiet.getPor_vegetales() == 0) {
                percentageArray[0] = (int) (((double) summVeg / (double) (dietReglist.size())) * 100);
            }
            else {
                percentageArray[0] = (int) (((double) summVeg / (double) (myDiet.getPor_vegetales() * dietReglist.size())) * 100);
            }

            if(myDiet.getPor_leche() == 0) {
                percentageArray[1] = (int) (((double) summLeche / (double) (dietReglist.size())) * 100);
            }
            else {
                percentageArray[1] = (int) (((double) summLeche / (double) (myDiet.getPor_leche() * dietReglist.size())) * 100);
            }

            if(myDiet.getPor_carnes() == 0) {
                percentageArray[2] = (int) (((double) summCarnes / (double) (dietReglist.size())) * 100);
            }
            else {
                percentageArray[2] = (int) (((double) summCarnes / (double) (myDiet.getPor_carnes() * dietReglist.size())) * 100);
            }

            if(myDiet.getPor_azucares() == 0) {
                percentageArray[3] = (int) (((double) summAzuc / (double) (dietReglist.size())) * 100);
            }
            else {
                percentageArray[3] = (int) (((double) summAzuc / (double) (myDiet.getPor_azucares() * dietReglist.size())) * 100);
            }

            if(myDiet.getPor_legumbres() == 0) {
                percentageArray[4] = (int) (((double) summLegum / (double) (dietReglist.size())) * 100);
            }
            else {
                percentageArray[4] = (int) (((double) summLegum / (double) (myDiet.getPor_legumbres() * dietReglist.size())) * 100);
            }

            if(myDiet.getPor_frutas() == 0) {
                percentageArray[5] = (int) (((double) summFrutas / (double) (dietReglist.size())) * 100);
            }
            else {
                percentageArray[5] = (int) (((double) summFrutas / (double) (myDiet.getPor_frutas() * dietReglist.size())) * 100);
            }

            if(myDiet.getPor_cereales() == 0) {
                percentageArray[6] = (int) (((double) summCer / (double) (dietReglist.size())) * 100);
            }
            else {
                percentageArray[6] = (int) (((double) summCer / (double) (myDiet.getPor_cereales() * dietReglist.size())) * 100);
            }

            if(myDiet.getPor_cereales() == 0) {
                percentageArray[7] = (int) (((double) summGrasas / (double) (dietReglist.size())) * 100);
            }
            else {
                percentageArray[7] = (int) (((double) summGrasas / (double) (myDiet.getPor_grasas() * dietReglist.size())) * 100);
            }
        }


        Bundle args = new Bundle();
        args.putIntArray("percentageArray", percentageArray);

        RecordDetailsListFragment recordDetListFoodFrag = new RecordDetailsListFragment();
        recordDetListFoodFrag.setArguments(args);
        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.recordDetailListFragParent, recordDetListFoodFrag, "recordDetailFragment");
        transaction.commit();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_record_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
