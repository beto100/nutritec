package com.betofortera.nutritec3;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.ListFragment;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by beto on 4/12/15.
 */
public class MyDayListFoodFragment extends ListFragment implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    protected DayFoodAdapter dayfoodAdapter;
    protected int[] maxFoodCounters = null;
    protected int[] registersCounters = null;
    protected int customLayout = -1;


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // set the listview adapter
        Bundle bundle = getArguments();
        if(bundle!=null) {
            maxFoodCounters = bundle.getIntArray("dynamicMaxFoodCounter");
            registersCounters = bundle.getIntArray("dynamicRegistersCounters");
            customLayout = bundle.getInt("customLay");
        }
        dayfoodAdapter = new DayFoodAdapter(getActivity(),maxFoodCounters,registersCounters);
        setListAdapter(dayfoodAdapter);
        getListView().setOnItemClickListener(this);
        getListView().setOnItemLongClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        TextView foodTitle = (TextView) view.findViewById(R.id.foodTitleDay);
        Bundle bundle = new Bundle();
        bundle.putString("foodTitle",foodTitle.getText().toString());
        bundle.putInt("itemPos",position);
        bundle.putInt("customLay",customLayout);

        FragmentManager manager = getActivity().getFragmentManager();
        MyDialogCreate myDialogCreate = new MyDialogCreate();
        myDialogCreate.setArguments(bundle);
        myDialogCreate.show(manager,"DialogCreate");
    }

    public void updateRegisterCounterRow(int[] newDataSource)
    {
        registersCounters = newDataSource;
        dayfoodAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

        Bundle bundle = new Bundle();
        bundle.putString("windowTitle","Alerta");
        bundle.putString("windowMessage","Estas seguro de reinicializar la cantidad de porciones de " + getResources().getStringArray(R.array.foodTitles)[position] +" del dia de hoy?");
        bundle.putInt("position",position);
        FragmentManager manager = getActivity().getFragmentManager();
        YesNoDialog myYesNoDialog = new YesNoDialog();
        myYesNoDialog.setArguments(bundle);
        myYesNoDialog.show(manager,"DialogCreate");

       // Toast.makeText(getActivity(),"On long click handled",Toast.LENGTH_LONG).show();

        return true;
    }
}

// Food Adapter
// ////////////////////////////////////////////////////
class DayFoodAdapter extends BaseAdapter
{
    ArrayList<SingleRow> list;
    Context context;
    int[] maxCounters = null;
    int[] registersCounters = null;

    public DayFoodAdapter(Context c, int[] maxCounters, int[] registersCounters)
    {
        context = c;
        this.maxCounters = maxCounters;
        this.registersCounters = registersCounters;
        list = new ArrayList<SingleRow>();
        Resources res = c.getResources();
        String[] foodTitles = res.getStringArray(R.array.foodTitles);
        int[] foodImages = {R.drawable.vegetables,R.drawable.milk,R.drawable.meat,R.drawable.sugar,R.drawable.legumes,R.drawable.fruits,R.drawable.cereal,R.drawable.fats};

        for(int i=0;i<8;i++) {
            list.add(new SingleRow(foodTitles[i],foodImages[i]));
        }
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    class MyViewHolder
    {
        ImageView foodImage;
        TextView foodTitle;
        TextView maxCounter;
        TextView registerCounter;

        MyViewHolder(View v) {
            this.foodImage = (ImageView) v.findViewById(R.id.foodImageDay);
            this.foodTitle = (TextView) v.findViewById(R.id.foodTitleDay);
            this.maxCounter = (TextView) v.findViewById(R.id.maxFoodCounter);
            this.registerCounter = (TextView) v.findViewById(R.id.registerFoodCounter);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;
        MyViewHolder holder = null;
        if(row == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.row_day, parent, false);
            holder = new MyViewHolder(row);
            row.setTag(holder);
        }
        else {
            holder = (MyViewHolder) row.getTag();
        }

        SingleRow temp = list.get(position);
        holder.foodTitle.setText(temp.title);
        holder.foodImage.setImageResource(temp.image);
        holder.maxCounter.setText(""+maxCounters[position]);
        if(this.maxCounters[position]<this.registersCounters[position]) {
            holder.registerCounter.setTextColor(Color.RED);
        }
        else{
            holder.registerCounter.setTextColor(Color.parseColor("#2196F3"));
        }
        holder.registerCounter.setText(""+registersCounters[position]);

        return row;
    }
}
