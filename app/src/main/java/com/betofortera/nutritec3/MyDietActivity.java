package com.betofortera.nutritec3;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.NavUtils;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


public class MyDietActivity extends ActionBarActivity implements MyDialogCreate.Communicator, YesNoDialog.YesNoCommunicator {

    protected int[] foodCounters;
    DBManager dbManager = null;
    Diet myDiet = null;
    TextView todayText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_diet);

        // set the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // add the list food fragment
        foodCounters = new int[8];
        todayText = (TextView) findViewById(R.id.initialDate);
        dbManager = new DBManager(this);
        SQLiteDatabase sqLiteDatabase = dbManager.getWritableDatabase();

        myDiet = dbManager.getYourDiet();
        foodCounters[0] = myDiet.getPor_vegetales();
        foodCounters[1] = myDiet.getPor_leche();
        foodCounters[2] = myDiet.getPor_carnes();
        foodCounters[3] = myDiet.getPor_azucares();
        foodCounters[4] = myDiet.getPor_legumbres();
        foodCounters[5] = myDiet.getPor_frutas();
        foodCounters[6] = myDiet.getPor_cereales();
        foodCounters[7] = myDiet.getPor_grasas();
        todayText.setText("Fecha Inicio: "+ TodayDate.getFormmatedDate(myDiet.getInitDate()));

        Bundle bundle = new Bundle();
        bundle.putIntArray("dynamicFoodCounter", foodCounters);
        bundle.putInt("customLay", R.layout.my_dialog_update);

        MyListFoodFragment listFoodFrag = new MyListFoodFragment();
        listFoodFrag.setArguments(bundle);
        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.foodListFragParentMyDiet, listFoodFrag, "foodFragmentMyDiet");
        transaction.commit();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my_diet, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.updateSetting) {

            Diet diet = new Diet(-1,"Dieta de la Luna", "2015-03-21","2015-06-21",foodCounters[0],foodCounters[1],foodCounters[2],foodCounters[3],foodCounters[4],foodCounters[5],foodCounters[6],foodCounters[7]);
            dbManager.updateDiet(diet);
            Toast.makeText(this,"Tu dieta ha sido actualizada con éxito!",Toast.LENGTH_LONG).show();
            return true;
        }
        else if(id == R.id.deleteSetting) {

            Bundle bundle = new Bundle();
            bundle.putString("windowTitle","Alerta");
            bundle.putInt("position",0);
            bundle.putString("windowMessage","Estas seguro de eliminar tu Dieta?");

            FragmentManager manager = getFragmentManager();
            YesNoDialog myYesNoDialog = new YesNoDialog();
            myYesNoDialog.setArguments(bundle);
            myYesNoDialog.show(manager,"DialogCreate");
            return true;

        }
        else if(id == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDialogMessage(String message, int pos) {

        if(message != null && !message.trim().equals("")) {
            try {
            foodCounters[pos] = Integer.parseInt(message.trim());
            FragmentManager manager = getFragmentManager();
            MyListFoodFragment listFragment = (MyListFoodFragment) manager.findFragmentByTag("foodFragmentMyDiet");
            listFragment.updateSomeRow(foodCounters);
            } catch (Exception e) {
                Toast.makeText(this,"Valor demasiado grande",Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onYesNoDialogMessage(boolean value, int position) {

        if(value) {
            //delete diet
            dbManager.deleteDiet();
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

}
