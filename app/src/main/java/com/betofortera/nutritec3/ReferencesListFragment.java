package com.betofortera.nutritec3;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by beto on 4/5/15.
 */
public class ReferencesListFragment extends ListFragment {

    protected ReferencesAdapter refAdapter;
    protected String[] _titlesArray = null;
    protected String[] _measuresArray = null;

    public static ReferencesListFragment getInstance(String[] titlesArray, String[] measuresArray)
    {
        ReferencesListFragment tempFrag = new ReferencesListFragment();
        Bundle bundle = new Bundle();
        bundle.putStringArray("titlesArray", titlesArray);
        bundle.putStringArray("measuresArray", measuresArray);
        tempFrag.setArguments(bundle);
        return tempFrag;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // set the listview adapter
        Bundle bundle = getArguments();
        if(bundle!=null) {
            _titlesArray = bundle.getStringArray("titlesArray");
            _measuresArray = bundle.getStringArray("measuresArray");
        }
        refAdapter = new ReferencesAdapter(getActivity(),_titlesArray,_measuresArray);
        setListAdapter(refAdapter);
    }
}



// Single Row References Class
// ////////////////////////////////////////////////////
class SingleReferencesRow
{
    public String portionTitle;
    public String portionMeasure;

    SingleReferencesRow(String portionTitle, String portionMeasure) {
        this.portionTitle = portionTitle;
        this.portionMeasure = portionMeasure;
    }
}



// References Adapter
// ////////////////////////////////////////////////////
class ReferencesAdapter extends BaseAdapter
{
    ArrayList<SingleReferencesRow> list;
    Context context;
    String[] _titlesArray = null;
    String[] _measuresArray = null;

    public ReferencesAdapter(Context c, String[] titlesArray, String[] measuresArray)
    {
        context = c;
        _titlesArray = titlesArray;
        _measuresArray = measuresArray;
        list = new ArrayList<SingleReferencesRow>();

        for(int i=0;i<_titlesArray.length;i++) {
            list.add(new SingleReferencesRow(_titlesArray[i],_measuresArray[i]));
        }
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    class MyViewHolder
    {
        TextView portionTitle;
        TextView portionMeasure;

        MyViewHolder(View v) {
            this.portionTitle = (TextView) v.findViewById(R.id.portionTitle);
            this.portionMeasure = (TextView) v.findViewById(R.id.portionMeasure);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;
        MyViewHolder holder = null;
        if(row == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.row_references, parent, false);
            holder = new MyViewHolder(row);
            row.setTag(holder);
        }
        else {
            holder = (MyViewHolder) row.getTag();
        }

        SingleReferencesRow temp = list.get(position);
        holder.portionTitle.setText(temp.portionTitle);
        holder.portionMeasure.setText(temp.portionMeasure);

        return row;
    }
}
