package com.betofortera.nutritec3;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import it.neokree.materialtabs.MaterialTab;
import it.neokree.materialtabs.MaterialTabHost;
import it.neokree.materialtabs.MaterialTabListener;


public class RecordsActivity extends ActionBarActivity implements MaterialTabListener {

    Toolbar toolbar;
    MaterialTabHost tabHost;
    ViewPager viewPager;
    TextView mainRecordTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_records);

        // set the toolbar
        mainRecordTitle = (TextView) findViewById(R.id.mainRecordTitle);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tabHost = (MaterialTabHost) findViewById(R.id.materialTabHostRecords);
        viewPager = (ViewPager) findViewById(R.id.viewPagerRecords);

        MyRecordsPagerAdapter pagerAdapter = new MyRecordsPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                switch (position) {
                    case 0:
                        mainRecordTitle.setText("Seleccione un día");
                        break;
                    case 1:
                        mainRecordTitle.setText("Seleccione una semana");
                        break;
                    case 2:
                        mainRecordTitle.setText("Seleccione un mes");
                        break;
                }
                tabHost.setSelectedNavigationItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        for (int i = 0; i < pagerAdapter.getCount(); i++) {
            tabHost.addTab(
                    tabHost.newTab()
                            .setText(pagerAdapter.getPageTitle(i))
                            .setTabListener(this)
            );

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_records, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if(id == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTabSelected(MaterialTab materialTab) {
        switch (materialTab.getPosition()) {
            case 0:
                mainRecordTitle.setText("Seleccione un día");
                break;
            case 1:
                mainRecordTitle.setText("Seleccione una semana");
                break;
            case 2:
                mainRecordTitle.setText("Seleccione un mes");
                break;
        }
        viewPager.setCurrentItem(materialTab.getPosition());
    }

    @Override
    public void onTabReselected(MaterialTab materialTab) {

    }

    @Override
    public void onTabUnselected(MaterialTab materialTab) {

    }


    class MyRecordsPagerAdapter extends FragmentPagerAdapter
    {

        public MyRecordsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment myFragment = null;
            switch(position) {

                case 0:
                    myFragment = RecordsListFragment.getInstance(position);
                    break;
                case 1:
                    myFragment = RecordsListFragment.getInstance(position);
                    break;
                case 2:
                    myFragment = RecordsListFragment.getInstance(position);
                    break;
            }
            return myFragment;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return getResources().getStringArray(R.array.timeTitles)[position];
        }
    }


}
